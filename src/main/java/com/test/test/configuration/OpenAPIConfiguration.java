package com.test.test.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class OpenAPIConfiguration {

    /**
     * Defines and configures the OpenAPI specification for the Ticket Management API.
     *
     * @return The OpenAPI object representing the API specification.
     */
    @Bean
    public OpenAPI defineOpenApi() {
        Server server = new Server();
        server.setUrl("http://localhost:8080");
        server.setDescription("Test");

        Contact myContact = new Contact();
        myContact.setName("Tsito");
        myContact.setEmail("tsito.odissi@yahoo.com");

        Info information = new Info()
                .title("Ticket Management")
                .version("1.0")
                .description("This API exposes endpoints to manage ticket.")
                .contact(myContact);
        return new OpenAPI().info(information).servers(List.of(server));
    }
}
