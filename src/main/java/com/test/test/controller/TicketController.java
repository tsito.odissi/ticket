package com.test.test.controller;

import com.test.test.dto.TicketDTO;
import com.test.test.services.TicketService;
import com.test.test.model.Ticket;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Controller class responsible for handling HTTP requests related to ticket management.
 */
@RestController
@RequestMapping("/api/ticket")
@Tag(name = "Ticket Management", description = "APIs for Ticket Management")
public class TicketController {
    @Autowired
    TicketService ticketService;

    /**
     * Retrieves all tickets.
     *
     * @return A list of all tickets.
     */
    @GetMapping
    @Operation(summary = "Get all tickets",
            description = "Find all tickets. The response list of all Tickets.")
    public ResponseEntity<List<TicketDTO>> getAllTickets() {
        return ResponseEntity.ok(ticketService.getAllTickets());
    }

    /**
     * Retrieves a ticket by its ID.
     *
     * @param id The ID of the ticket to retrieve.
     * @return The ticket with the specified ID, if found.
     */
    @GetMapping("/{id}")
    @Operation(summary = "Get a ticket",
            description = "Get a ticket by ID")
    public ResponseEntity<TicketDTO> getTicketById(@PathVariable int id) {
        Optional<TicketDTO> ticketDTO = ticketService.findById(id);
        return ticketDTO.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Saves a new ticket.
     *
     * @param ticket The ticket to be saved.
     * @return The saved ticket.
     */
    @PostMapping("/save")
    @Operation(summary = "Save",
            description = "Save a ticket")
    public ResponseEntity<TicketDTO> createTicket(@Valid  @RequestBody Ticket ticket) {
            return ResponseEntity.ok(ticketService.saveTicket(ticket));
    }

    /**
     * Updates an existing ticket.
     *
     * @param id The ID of the ticket to be updated.
     * @param ticket The updated ticket information.
     * @return The updated ticket.
     */
    @PutMapping("/{id}")
    @Operation(summary = "Update a ticket",
            description = "Update a existing ticket. The response is updated Ticket object.")
    public ResponseEntity<TicketDTO> updateTicket(@PathVariable int id, @RequestBody Ticket ticket) {
        ticket.setIdTicket(id);
        return ResponseEntity.ok(ticketService.saveTicket(ticket));
    }

    /**
     * Assigns a ticket to a user.
     *
     * @param id The ID of the ticket to be assigned.
     * @param userId The ID of the user to whom the ticket will be assigned.
     */
    @PutMapping("/{id}/assign/{userId}")
    @Operation(summary = "Update a ticket",
            description = "assign a ticket for an user.")
    public void assignTicket(@PathVariable int id, @PathVariable int userId) {
        ticketService.assignTicket(id, userId);
    }

    /**
     * Deletes a ticket by its ID.
     *
     * @param id The ID of the ticket to be deleted.
     */
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a ticket",
            description = "Delete a ticket By id.")
    public void deleteTicket(@PathVariable int id) {
        ticketService.deleteTicketById(id);
    }
}
