package com.test.test.controller;

import com.test.test.dto.TicketDTO;
import com.test.test.dto.UserDTO;
import com.test.test.mapper.UserMapper;
import com.test.test.model.Ticket;
import com.test.test.model.User;
import com.test.test.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controller class responsible for handling HTTP requests related to user management.
 */
@RestController
@RequestMapping("/api/user")
@Tag(name = "User", description = "APIs for User")
public class UserController {

  @Autowired
  UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  /**
   * Retrieves all users.
   *
   * @return A list of all users.
   */
  @GetMapping
  @Operation(summary = "Get all users APIs",
          description = "A list of all users.")
  public ResponseEntity<List<UserDTO>> getAllUsers() {
    return ResponseEntity.ok(userService.getAllUsers());
  }

  @GetMapping("/{id}")
  @Operation(summary = "Get a user by Id APIs",
          description = "Find user by ID.")
  public ResponseEntity<UserDTO> findById(@PathVariable int id) {
      Optional<UserDTO> user = userService.findById(id);
      return user.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
  }

  /**
   * Retrieves all tickets assigned to a user by user ID.
   *
   * @param id The ID of the user.
   * @return A list of tickets assigned to the user with the specified ID.
   */
  @GetMapping("/{id}/ticket")
  @Operation(summary = "Get a user by ID",
          description = "Retrieves all tickets assigned to a user by user ID.")
  public ResponseEntity<List<TicketDTO>> getUserTickets(@PathVariable int id) {
    try {
      return  ResponseEntity.ok(userService.getUserTickets(id));
    }catch (IllegalArgumentException e){
      return ResponseEntity.notFound().build();
    }

  }

  /**
   * Saves a new user.
   *
   * @param user The user to be saved.
   * @return The saved user.
   */
  @PostMapping("/save")
  @Operation(summary = "Save a user APIs",
          description = "Saves a new user")
  public ResponseEntity<UserDTO> createUser(@Valid @RequestBody User user) {
    return ResponseEntity.ok(userService.saveUser(user));
  }

  /**
   * Updates an existing user.
   *
   * @param id The ID of the user to be updated.
   * @param user The updated user information.
   * @return The updated user.
   */
  @PutMapping("/{id}")
  @Operation(summary = "Update a user",
          description = "Updates an existing user")
  public ResponseEntity<UserDTO> updateUser(@PathVariable int id, @RequestBody User user) {
    user.setIdUser(id);
    return ResponseEntity.ok(userService.saveUser(user));
  }

  /**
   * Deletes a user by its ID.
   *
   * @param id The ID of the user to be deleted.
   */
  @DeleteMapping("/{id}")
  @Operation(summary = "Delete a user",
          description = "Deletes a user by its ID")
  public void deleteUser(@PathVariable int id) {
    userService.deleteUserById(id);
  }
}
