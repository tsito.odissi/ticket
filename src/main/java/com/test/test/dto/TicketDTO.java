package com.test.test.dto;

import com.test.test.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TicketDTO {
    private int idTicket;
    private String title;
    private String description;
    private User user;
    private String status;
}
