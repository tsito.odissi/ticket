package com.test.test.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDTO {
    private int idUser;
    private String userName;
    private String userMail;

    public UserDTO() {

    }
}
