package com.test.test.mapper;

import com.test.test.dto.TicketDTO;
import com.test.test.model.Ticket;

public class TicketMapper {
    /**
     *  Convert Ticket JPA Entity into TicketDto
     */
    public static TicketDTO mapToTicketDTO(Ticket ticket){
        return new TicketDTO(
                ticket.getIdTicket(),
                ticket.getTitle(),
                ticket.getDescription(),
                ticket.getUser(),
                ticket.getStatut().toString());
    }

    public static Ticket mapToTicket(TicketDTO ticketDTO){
        return new Ticket(
                ticketDTO.getIdTicket(),
                ticketDTO.getTitle(),
                ticketDTO.getDescription(),
                ticketDTO.getUser(),
                ticketDTO.getStatus()
        );
    }
}
