package com.test.test.mapper;

import com.test.test.dto.UserDTO;
import com.test.test.model.User;

public class UserMapper {
    /**
     *  Convert User JPA Entity into UserDto
     */
    public static UserDTO mapToUserDto(User user){
        return new UserDTO(
                user.getIdUser(),
                user.getUserName(),
                user.getUserMail()
        );
    }

    public static User mapToUser(UserDTO userDto){
        return new User(
                userDto.getIdUser(),
                userDto.getUserName(),
                userDto.getUserMail()
        );
    }
}
