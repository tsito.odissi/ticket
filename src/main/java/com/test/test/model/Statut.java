package com.test.test.model;

public enum Statut {
  EN_COURS,
  TERMINE,
  ANNULE
}
