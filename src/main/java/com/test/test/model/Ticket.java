package com.test.test.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Column;
import jakarta.persistence.Table;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Enumerated;
import jakarta.persistence.EnumType;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Entity
@Table(name = "Ticket")
@Data
public class Ticket {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int idTicket;
  @Column(name="title")
  @NotBlank(message = "Title is required")
  private String title;
  @Column(name="description")
  @NotBlank(message = "Title is required")
  private String description;
  @ManyToOne
  @JoinColumn(name = "IDUSERS")
  private User user;
  @Enumerated(EnumType.STRING)
  private Statut statut;

  public Ticket(int idTicket, String title, String description, User user, String status) {
  }

  public Ticket() {

  }
}
