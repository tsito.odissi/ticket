package com.test.test.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Column;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Table(name = "Users")
@Data
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int idUser;
  @Column(name="username")
  @NotBlank(message = "User name is required")
  private String userName;
  @Column(name="usermail")
  @NotBlank(message = "User mail is required")
  @Email(message = "Email should be valid")
  private String userMail;

  public User(int idUser, String userName, String userMail) {
  }

  public User(){}
}
