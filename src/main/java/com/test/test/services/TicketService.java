package com.test.test.services;

import com.test.test.dto.TicketDTO;
import com.test.test.mapper.TicketMapper;
import com.test.test.model.Ticket;
import com.test.test.model.User;
import com.test.test.dao.TicketRepository;
import com.test.test.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TicketService {
    @Autowired
    private TicketRepository ticketRepository;
    @Autowired
    private UserRepository userRepository;

    public List<TicketDTO> getAllTickets(){
        return ticketRepository.findAll().stream().map(TicketMapper::mapToTicketDTO).collect(Collectors.toList());
    }

    public TicketDTO saveTicket(Ticket ticket){
        return TicketMapper.mapToTicketDTO(ticketRepository.save(ticket));
    }

    public Optional<TicketDTO> findById(int id){
        return Optional.of(TicketMapper.mapToTicketDTO(ticketRepository.findById(id).get()));
    }

    public void deleteTicketById(int id){
        ticketRepository.deleteById(id);
    }

    public Ticket assignTicket(int ticketId, int userId){
        Optional<Ticket> optionalTicket = ticketRepository.findById(ticketId);
        Optional<User> optionalUser = userRepository.findById(userId);
        if(optionalTicket.isPresent() && optionalUser.isPresent()){
            Ticket ticket = optionalTicket.get();
            User user = optionalUser.get();
            ticket.setUser(user);
            return ticketRepository.save(ticket);
        }else{
            throw new IllegalArgumentException("Ticket or user not found");
        }
    }
}
