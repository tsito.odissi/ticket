package com.test.test.services;

import com.test.test.dto.TicketDTO;
import com.test.test.dto.UserDTO;
import com.test.test.mapper.TicketMapper;
import com.test.test.mapper.UserMapper;
import com.test.test.model.Ticket;
import com.test.test.model.User;
import com.test.test.dao.TicketRepository;
import com.test.test.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TicketRepository ticketRepository;

    public List<UserDTO> getAllUsers(){
            return userRepository.findAll().stream().map(UserMapper::mapToUserDto).collect(Collectors.toList());
    }

    public UserDTO saveUser(User user){
        return UserMapper.mapToUserDto(userRepository.save(user));
    }

    public Optional<UserDTO> findById(int id){
        return Optional.of(UserMapper.mapToUserDto(userRepository.findById(id).get()));
    }

    public void deleteUserById(int id){
        userRepository.deleteById(id);
    }

    public List<TicketDTO> getUserTickets(int idUser){
        Optional<User> optionalUser = userRepository.findById(idUser);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            return ticketRepository.findByUser(user).stream().map(TicketMapper::mapToTicketDTO).collect(Collectors.toList());
        }else{
            throw new IllegalArgumentException("User not found");
        }
    }

}
