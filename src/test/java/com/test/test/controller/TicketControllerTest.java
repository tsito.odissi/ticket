package com.test.test.controller;

import com.test.test.mapper.TicketMapper;
import com.test.test.model.Ticket;
import com.test.test.services.TicketService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TicketControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private TicketService ticketService;

    @InjectMocks
    private TicketController ticketController;

    @Test
    public void testCreateTicket() throws Exception {
        Ticket ticket = new Ticket();
        ticket.setTitle("Test Ticket");
        ticket.setDescription("Test Description");

        when(ticketService.saveTicket(any(Ticket.class))).thenReturn(TicketMapper.mapToTicketDTO(ticket));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/ticket/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"title\":\"Test Ticket\",\"description\":\"Test Description\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testUpdateTicket() throws Exception {
        Ticket ticket = new Ticket();
        ticket.setIdTicket(1);
        ticket.setTitle("Updated Ticket");
        ticket.setDescription("Updated Description");
        when(ticketService.saveTicket(ticket)).thenReturn(TicketMapper.mapToTicketDTO(ticket));
        mockMvc.perform(MockMvcRequestBuilders.put("/api/ticket/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"title\":\"Updated Ticket\",\"description\":\"Updated Description\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testDeleteTicket() throws Exception {
        doNothing().when(ticketService).deleteTicketById(1);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/ticket/1"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
