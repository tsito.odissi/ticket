package com.test.test.service;

import com.test.test.dao.TicketRepository;
import com.test.test.dto.TicketDTO;
import com.test.test.mapper.TicketMapper;
import com.test.test.model.Ticket;
import com.test.test.services.TicketService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TicketServiceTest {
    @Mock
    private TicketRepository ticketRepository;

    @InjectMocks
    private TicketService ticketService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllTickets() {
        List<Ticket> tickets = Collections.singletonList(new Ticket());
        when(ticketRepository.findAll()).thenReturn(tickets);
        List<TicketDTO> result = ticketService.getAllTickets();
        assertEquals(tickets.size(), result.size());
    }

    @Test
    public void testGetTicketById() {
        Ticket ticket = new Ticket();
        ticket.setIdTicket(1);
        Optional<Ticket> optionalTicket = Optional.of(ticket);
        when(ticketRepository.findById(1)).thenReturn(optionalTicket);
        Optional<TicketDTO> result = ticketService.findById(1);
        assertEquals(ticket.getIdTicket(), result.get().getIdTicket());
    }

    @Test
    public void testCreateTicket() {
        Ticket ticket = new Ticket();
        when(ticketRepository.save(ticket)).thenReturn(ticket);
        TicketDTO result = ticketService.saveTicket(ticket);
        assertEquals(ticket, TicketMapper.mapToTicket(result));
    }

    @Test
    public void testUpdateTicket() {
        Ticket ticket = new Ticket();
        ticket.setIdTicket(1);
        when(ticketRepository.save(ticket)).thenReturn(ticket);
        TicketDTO result = ticketService.saveTicket(ticket);
        assertEquals(TicketMapper.mapToTicketDTO(ticket),  result);
    }

    @Test
    public void testDeleteTicket() {
        doNothing().when(ticketRepository).deleteById(1);
        ticketService.deleteTicketById(1);
        verify(ticketRepository, times(1)).deleteById(1);
    }
}