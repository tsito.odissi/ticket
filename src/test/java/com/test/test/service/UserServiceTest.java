package com.test.test.service;

import com.test.test.dao.UserRepository;
import com.test.test.dto.UserDTO;
import com.test.test.mapper.UserMapper;
import com.test.test.model.User;
import com.test.test.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllUsers() {
        List<User> users = Collections.singletonList(new User());
        when(userRepository.findAll()).thenReturn(users);
        List<UserDTO> result = userService.getAllUsers();
        assertEquals(users.size(), result.size());
    }

    @Test
    public void testGetUserById() {
        User user = new User();
        user.setIdUser(1);
        Optional<User> optionalUser = Optional.of(user);
        when(userRepository.findById(1)).thenReturn(optionalUser);
        Optional<UserDTO> result = userService.findById(1);
        assertEquals(user.getIdUser(), result.get().getIdUser());
    }

    @Test
    public void testCreateUser() {
        User user = new User();
        when(userRepository.save(user)).thenReturn(user);
        User result = userRepository.save(user);
        assertEquals(user, result);
    }

    @Test
    public void testUpdateUser() {
        User user = new User();
        user.setIdUser(1);
        when(userRepository.save(user)).thenReturn(user);
        User result = UserMapper.mapToUser(userService.saveUser(user));
        assertEquals(user, result);
    }

    @Test
    public void testDeleteUser() {
        doNothing().when(userRepository).deleteById(1);
        userService.deleteUserById(1);
        verify(userRepository, times(1)).deleteById(1);
    }
}
