DROP TABLE IF EXISTS users;
CREATE TABLE users (
    idUsers INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    usermail VARCHAR(255) NOT NULL
);
DROP TABLE IF EXISTS ticket;
CREATE TABLE ticket (
    idticket INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    idUsers INT,
    FOREIGN KEY (idUsers) REFERENCES users(idUsers)
);